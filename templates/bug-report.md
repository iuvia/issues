
# Bug Title
<!--- Provide a one-line summary of the issue in the Title above -->

## Description
<!-- Detailed description of the bug. Explain to us what happened, what were you trying to do and what is the error that you faced. Attach to this all the information that you have, you can add screenshots and any error messages that you got (if any) -->

## Steps to Reproduce
<!-- Provide a clear list of steps to reproduce the error -->
1. Start point
2. Step 1
3. Step 2
4. Issue 

## Expected Behavior
<!--- Tell us what should happen -->

## Context (Environment)
<!--- Let us know your full setup. Are you using IUVIA Device? Are you using the IUVIA platform over your own hardware? Did you find this error while working on a development environment? -->

<!-- Add full information of the system setup (hardware, OS, IUVIA release versions, etc.) over which this error happened -->

## Possible Solution
<!--- Not obligatory, but suggest a fix/reason for the bug, -->